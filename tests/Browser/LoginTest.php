<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    //use DatabaseMigrations;

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function ($browser) {
            $browser->maximize()
                ->visit('/login')
                ->type('email', 'alibayat73@gmail.com')
                ->type('password', '123456789')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }
}
