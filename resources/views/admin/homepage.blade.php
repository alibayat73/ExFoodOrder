@extends("layouts.classresTemplate")

@section('title') Administration Panel @endsection

@section('content')
    <a href="{{route('admin.indexcity')}}">
        <button type="submit" class="btn btn-warning">Cities</button>
    </a>

    <a href="{{route('admin.indexrestaurant')}}">
        <button type="submit" class="btn btn-dark">Restaurants</button>
    </a>

    <a href="{{route('admin.indexcategory')}}">
        <button type="submit" class="btn btn-success">Food Categories</button>
    </a>

    <a href="{{route('admin.indexfood')}}">
        <button type="submit" class="btn btn-info">Foods</button>
    </a>

    <a href="{{route('admin.indexuser')}}">
        <button type="submit" class="btn btn-primary">Users</button>
    </a>
@endsection
