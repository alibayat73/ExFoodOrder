@extends("layouts.classresTemplate")

@section('title') Edit User @endsection

@section('content')

    <form action="{{route('admin.updateuser',['user'=>$user])}}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div id="name" class="form-group">
                <label for="name"><h2>Editing Name</h2></label>
                <input type="text" name="name" class="form-control" placeholder="Ex: JohnCK98"
                       value="{{$user->name}}">
                <small class="form-text text-muted">Edit Username</small>
            </div>
        </div>

        <div class="row">
            <div id="email" class="form-group">
                <label for="email"><h2>Editing Email</h2></label>
                <input type="text" name="email" class="form-control" placeholder="Ex: JohnCK98@google.com"
                       value="{{$user->email}}">
                <small class="form-text text-muted">Edit E-Mail</small>
            </div>
        </div>

        <div class="row">
            <div id="mobile" class="form-group">
                <label for="mobile"><h2>Editing Mobile</h2></label>
                <input type="number" name="mobile" class="form-control" placeholder="Ex: 0912345678"
                       value="{{$user->mobile}}">
                <small class="form-text text-muted">Edit Mobile</small>
            </div>
        </div>

        <div class="row">
            <div id="status" class="form-group">
                <label for="status"><h2>Edit Status</h2></label>
                <select name="status" class="form-control">
                    <option value="0">0</option>
                    <option value="1">1</option>
                </select>
                <small class="form-text text-muted">Edit Status</small>
            </div>
        </div>

        @can('before',$user)
            <div class="row">
                <div id="role_id" class="form-group">
                    <label for="role_id"><h2>Edit Role</h2></label>
                    <select name="role_id" class="form-control">
                        <option value="0">Admin</option>
                        <option value="2">Restaurateur</option>
                        <option value="1">Customer</option>
                    </select>
                    <small class="form-text text-muted">Edit Role_ID</small>
                </div>
            </div>
        @endcan

        <div class="row">
            <div id="address" class="form-group">
                <label for="address"><h2>Editing Address</h2></label>
                <textarea name="address" class="form-control" placeholder="Ex: Tehran Pars, No 23" value="{{$user->address}}"></textarea>
                <small class="form-text text-muted">Edit Address</small>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
