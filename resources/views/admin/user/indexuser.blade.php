@extends("layouts.classresTemplate")

@section('title') Users @endsection

@section('content')

    <table class="table table-hover table-striped">
        <thead class="thead-light">
        <tr>
            <th >#</th>
            <th >Username</th>
            <th >E-Mail</th>
            <th >E-Mail Verified At</th>
            <th >Mobile</th>
            <th >Status</th>
            <th >Role_ID</th>
            <th >Address</th>
            <th >Created At</th>
            <th >Updated At</th>
            <th ></th>
            <th ></th>
        </tr>
        </thead>

        <tbody class="table-bordered">
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->email_verified_at}}</td>
                <td>{{$user->mobile}}</td>
                <td>{{$user->status}}</td>
                @if($user->role_id == 1)
                    <td>Customer</td>
                @elseif($user->role_id == 0)
                    <td>Admin</td>
                @else
                    <td>Restaurateur</td>
                @endif
                <td>{{$user->address}}</td>
                <td>{{jdate($user->created_at)->format('Y/m/d H:i')}}</td>
                <td>{{jdate($user->updated_at)->format('Y/m/d H:i')}}</td>
                <td>
                    <a href="{{route('admin.edituser',['user'=>$user])}}">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </a>
                </td>
                <td>
                    <form action="{{route('admin.destroyuser',['user'=>$user])}}" method="POST">
                        @CSRF
                        @method('DELETE')
                        <button onclick="return confirm('You Are About To Delete This User. This Can Not Be Undone. Are You Sure?')"
                                type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
@endsection
