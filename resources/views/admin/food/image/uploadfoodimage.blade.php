@extends("layouts.classresTemplate")

@section('title') Upload Food Image @endsection

@section('content')

    <form action="{{route('admin.savefoodimage',['food_id'=>$food->id])}}" method="post" enctype="multipart/form-data">
        @CSRF
        <div class="row">
            <div id="food_image" class="form-group">
                <label for="food_image"><h2>Food Image For {{$food->title}}</h2></label>
                <input type="file" name="food_image" class="form-control">
                <small class="form-text text-muted">Set Food Image Here</small>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

@endsection
