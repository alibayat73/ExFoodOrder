@extends("layouts.classresTemplate")

@section('title') Foods @endsection

@section('content')

    @if(session()->has('done'))
        <div class="table">
            <div class="alert alert-success">
                {{session('done')}}
            </div>
        </div>
    @elseif(session()->has('deleted'))
        <div class="table">
            <div class="alert alert-danger">
                {{session('deleted')}}
            </div>
        </div>
    @endif

    <table class="table table-hover table-striped">
        <thead class="thead-light">
        <tr>
            <th >#</th>
            <th >Title</th>
            <th >Price</th>
            <th >Restaurant_ID</th>
            <th >Category_ID</th>
            <th >Images</th>
            <th >Created At</th>
            <th >Updated At</th>
            <th >Add Image</th>
            <th ></th>
            <th ></th>
        </tr>
        </thead>

        <tbody class="table-bordered">
            @foreach($foods as $food)
                <tr>
                    <td>{{$food->id}}</td>
                    <td>{{$food->title}}</td>
                    <td>{{$food->price}}</td>
                    <td>{{$food->restaurant_id}}</td>
                    <td>{{$food->category_id}}</td>
                    <td>
                        @foreach($food->images as $food_image)
                            <img class="img-fluid img-thumbnail" src="{{url('/storage/'.$food_image->path)}}">
                        @endforeach
                    </td>
                    <td>{{jdate($food->created_at)->format('Y/m/d H:i')}}</td>
                    <td>{{jdate($food->updated_at)->format('Y/m/d H:i')}}</td>
                    <td>
                        <form action="{{route('admin.uploadfoodimage',['food_id'=>$food->id])}}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-dark">ADD</button>
                        </form>
                    </td>
                    <td>
                        <a href="{{route('admin.editfood',['id'=>$food->id])}}">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </a>
                    </td>
                    <td>
                        <form action="{{route('admin.destroyfood',['id'=>$food->id])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button onclick="return confirm('You Are About To Delete This Category. Are You Sure?')"
                                    type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>

    <a href="{{route('admin.createfood')}}">
        <button type="submit" class="btn btn-info">Add a New Food</button>
    </a>
@endsection
