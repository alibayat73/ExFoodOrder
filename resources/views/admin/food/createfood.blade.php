@extends("layouts.classresTemplate")

@section('title') Create Food @endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('admin.storefood')}}" method="POST">
        @CSRF
        <div class="row">
            <div id="title" class="form-group">
                <label for="title"><h2>Title</h2></label>
                <input type="text" name="title" value="{{old('title')}}" class="form-control" placeholder="Ex: Cheesburger">
                <small class="form-text text-muted">Set Food Title Here</small>
            </div>
        </div>

        <div class="row">
            <div id="price" class="form-group">
                <label for="price"><h2>Price</h2></label>
                <input type="number" name="price" value="{{old('price')}}" class="form-control" placeholder="Ex: 27000">
                <small class="form-text text-muted">Set Food Price Here</small>
            </div>
        </div>

        <div class="row">
            <div id="restaurant_id" class="form-group">
                <label for="restaurant_id"><h2>Restaurant</h2></label><br>
                <select name="restaurant_id">
                    @foreach(\App\Restaurant::get() as $restaurant)
                        <option value="{{$restaurant->id}}">{{$restaurant->title}}</option>
                    @endforeach
                </select>
                <small class="form-text text-muted">Choose Restaurant Here</small>
            </div>
        </div>

        <div class="row">
            <div id="category_id" class="form-group">
                <label for="category_id"><h2>Category</h2></label><br>
                <select name="category_id">
                    @foreach(\App\Category::get() as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
                <small class="form-text text-muted">Choose Category Here</small>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

@endsection
