@extends("layouts.classresTemplate")

@section('title') Cities @endsection

@section('content')

    @if(session()->has('done'))
        <div class="table">
            <div class="alert alert-success">
                {{session('done')}}
            </div>
        </div>
        @elseif(session()->has('deleted'))
        <div class="table">
            <div class="alert alert-danger">
                {{session('deleted')}}
            </div>
        </div>
    @endif

    <table class="table table-hover table-striped">
        <thead class="thead-light">
        <tr>
            <th >#</th>
            <th >Title</th>
            <th >Created At</th>
            <th >Updated At</th>
            <th ></th>
            <th ></th>
        </tr>
        </thead>

        <tbody class="table-bordered">
        @foreach($cities as $city)
            <tr>
                <td>{{$city->id}}</td>
                <td>{{$city->title}}</td>
                <td>{{jdate($city->created_at)->format('Y/m/d H:i')}}</td>
                <td>{{jdate($city->updated_at)->format('Y/m/d H:i')}}</td>
                <td>
                    <a href="{{route('admin.editcity',['id'=>$city->id])}}">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </a>
                </td>
                <td>
                    <form action="{{route('admin.destroycity',['id'=>$city->id])}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button onclick="return confirm('You Are About To Delete This City. Are You Sure?')"
                                type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

    <a href="{{route('admin.createcity')}}">
        <button type="submit" class="btn btn-info">Add a New City</button>
    </a>
@endsection
