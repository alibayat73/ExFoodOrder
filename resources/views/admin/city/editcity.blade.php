@extends("layouts.classresTemplate")

@section('title') Edit City @endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('admin.updatecity',['id'=>$city->id])}}" method="post">
        @csrf
        @method('PUT')

        <div class="row">
            <div id="title" class="form-group">
                <label for="title"><h2>Editing Title</h2></label>
                <input type="text" name="title" class="form-control" placeholder="Ex: Tehran"
                       value="{{$city->title}}">
                <small class="form-text text-muted">Edit The Name Of The City That Your Restaurant Is In It.</small>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
