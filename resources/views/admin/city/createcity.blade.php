@extends("layouts.classresTemplate")

@section('title') Create City @endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('admin.storecity')}}" method="post">
        @csrf
        <div class="row">
            <div id="title" class="form-group">
                <label for="title"><h2>Title</h2></label>
                <input type="text" value="{{old('title')}}" name="title" class="form-control" placeholder="Ex: Tehran">
                <small class="form-text text-muted">Set City Name Here.</small>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
