@extends("layouts.classresTemplate")

@section('title') Edit Restaurant @endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{route('admin.updaterestaurant',['id'=>$restaurant->id] ) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @CSRF

        <div class="row">
            <div id="title" class="form-group">
                <label for="title"><h2>Title</h2></label>
                <input type="text" name="title" value="{{$restaurant->title}}" class="form-control" placeholder="Ex: Perperook">
                <small class="form-text text-muted">Edit Restaurant Name Here</small>
            </div>
        </div> <!-- Title -->

        <div class="row">
            <div id="logo" class="form-group">
                <label for="logo"><h2>Logo</h2></label>
                <input type="file" name="logo" class="form-control">
                <small class="form-text text-muted">Edit Restaurant Logo Here</small>
            </div>
        </div> <!-- Logo -->

        <div class="row">
            <div id="address" class="form-group">
                <label for="address"><h2>Address</h2></label>
                <textarea name="address" class="form-control"
                          placeholder="Ex: Baker St. No.221">{{$restaurant->address}}</textarea>
                <small class="form-text text-muted">Edit Restaurant Address Here</small>
            </div>
        </div> <!-- Address -->

        <div class="row">
            <div id="phone" class="form-group">
                <label for="phone"><h2>Phone Number</h2></label>
                <input type="number" name="phone" value="{{$restaurant->phone}}" class="form-control" placeholder="02177123456">
                <small class="form-text text-muted">Edit Phone Number Here</small>
            </div>
        </div> <!-- Phone Number -->

        <div class="row">
            <div id="city_id" class="form-group">
                <label for="city_id"><h2>City</h2></label>
                <select name="city_id">
                    @foreach(\App\City::get() as $city)
                        <option value="{{$city->id}}"> {{$city->title}} </option>
                    @endforeach
                </select>
                <small class="form-text text-muted">Change City Here</small>
            </div>
        </div> <!-- City ID -->

        <div class="row">
            <div id="opening_time" class="form-group">
                <label for="opening_time"><h2>Opening Time</h2></label>
                <input type="number" name="opening_time" value="{{$restaurant->opening_time}}" class="form-control">
                <small class="form-text text-muted">Edit Opening Time Here</small>
            </div>
        </div> <!-- Opening -->

        <div class="row">
            <div id="closing_time" class="form-group">
                <label for="closing_time"><h2>Closing Time</h2></label>
                <input type="number" name="closing_time" value="{{$restaurant->closing_time}}" class="form-control">
                <small class="form-text text-muted">Edit Closing Time Here</small>
            </div>
        </div> <!-- Closing -->

        <div class="row">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div> <!-- Submit Button -->

    </form>
@endsection
