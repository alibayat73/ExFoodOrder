@extends("layouts.classresTemplate")

@section('title') Restaurant @endsection

@section('content')

    @if(session()->has('done'))
        <div class="table">
            <div class="alert alert-success">
                {{session('done')}}
            </div>
        </div>
    @elseif(session()->has('deleted'))
        <div class="table">
            <div class="alert alert-danger">
                {{session('deleted')}}
            </div>
        </div>
    @endif

    <table class="table table-hover table-striped">
        <thead class="thead-light">
            <tr>
                <th >#</th>
                <th >Title</th>
                <th >Logo</th>
                <th >Address</th>
                <th >Phone</th>
                <th >City</th>
                <th >Opening</th>
                <th >Closing</th>
                <th >Created At</th>
                <th >Updated At</th>
                <th ></th>
                <th ></th>
            </tr>
        </thead>

        <tbody class="table-bordered">
            @foreach($restaurants as $restaurant)
                <tr>
                    <td>{{$restaurant->id}}</td>
                    <td>{{$restaurant->title}}</td>
                    <td>
                        <img class="img-fluid img-thumbnail" src="{{url('/storage/'.$restaurant->logo)}}">
                    </td>
                    <td>{{$restaurant->address}}</td>
                    <td>{{$restaurant->phone}}</td>
                    <td>{{$restaurant->city->title}}</td>
                    <td>{{$restaurant->opening_time}}</td>
                    <td>{{$restaurant->closing_time}}</td>
                    <td>{{jdate($restaurant->created_at)->format('Y/m/d H:i')}}</td>
                    <td>{{jdate($restaurant->updated_at)->format('Y/m/d H:i')}}</td>
                    <td>
                        <a href="{{route('admin.editrestaurant',['id'=>$restaurant->id])}}">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </a>
                    </td>
                    <td>
                        <form action="{{route('admin.destroyrestaurant',['id'=>$restaurant->id])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button onclick="return confirm('You Are About To Delete This Restaurant. Are You Sure?')"
                                    type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>

    <a href="{{route('admin.createrestaurant')}}">
        <button type="submit" class="btn btn-info">Add a New Restaurant</button>
    </a>
@endsection
