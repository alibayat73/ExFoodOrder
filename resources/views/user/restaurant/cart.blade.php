@extends('layouts.classresTemplate')

@section('content')

    @if(session()->has('item_deleted'))
        <div class="table">
            <div class="alert alert-danger">
                {{session('item_deleted')}}
            </div>
        </div>
    @elseif(session()->has('all_items_deleted'))
        <div class="table">
            <div class="alert alert-danger">
                {{session('all_items_deleted')}}
            </div>
        </div>
    @elseif(session()->has('submited'))
        <div class="table">
            <div class="alert alert-success">
                {{session('submited')}}
            </div>
        </div>
    @endif

    <div class="row"><h1>Cart</h1></div>
    <div class="row">
        <table class="table table-hover table-bordered">
            <tr>
                <td>#</td>
                <td>Food Title</td>
                <td>Category</td>
                <td>Price</td>
                <td>Delete</td>
            </tr>
            <?php
                $i = 1;
                $total = 0;
            ?>
            @foreach($carts as $cart)
                <?php
                    $food = \App\Food::find($cart->food_id);
                    $total += $food->price;
                ?>
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$food->title}}</td>
                    <td>{{$food->category->title}}</td>
                    <td>{{number_format($food->price)}}</td>
                    <td>
                        <form action="{{route('user.cartdestroyitem')}}" method="POST">
                            @CSRF
                            @method('DELETE')
                            <input type="hidden" name="cart_item" value="{{$cart->id}}">
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3">Total Price</td>
                <td>{{number_format($total)}}</td>
                <td>
                    <form action="{{route('user.cartdestroyall')}}" method="POST">
                        @CSRF
                        <button type="submit" class="btn btn-dark">Delete All</button>
                    </form>
                </td>
            </tr>
        </table>
    </div>

    <div class="row">
        <form action="{{route('user.cartsubmit')}}" method="POST">
            @CSRF
            <button class="btn btn-success">Submit Order</button>
        </form>

    </div>
@endsection
