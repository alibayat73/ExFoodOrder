@extends("layouts.classresTemplate")

@section('title') Food List @endsection

@section('content')

    <div class="row">
        @foreach($foods as $food)
            <div class="col-3">
                <div class="row">
                    @if(App\FoodImage::where('food_id',$food->id)->first() != null)
                        @foreach(\App\FoodImage::where('food_id',$food->id)->get() as $food_image)
                            <img class="img-fluid img-thumbnail" width="200px" src="{{url('/storage/'.$food_image->path)}}">
                            <br>
                        @endforeach
                    @else
                        <img class="img-fluid img-thumbnail" width="200px" src="{{url('nophoto.png')}}"> <br>
                    @endif
                </div>
                <div class="row">
                    <h5>{{$food->title}} : {{$food->price}}</h5>
                </div>
                <div class="row">
                    <h6>Res: {{$food->restaurant->title}}</h6>
                </div>
                <div class="row">
                    <a href="{{route('user.addtocart',['food_id'=>$food->id])}}">
                        <button type="submit" class="btn btn-primary">Add To Cart</button>
                    </a><br>
                </div>
                <div class="row">
                    <h5>Average = {{\App\FoodRate::where('food_id',$food->id)->avg('rate')}}</h5>
                </div>
                <div class="row">
                    @auth
                        <form action="{{route('user.rate')}}" method="POST">
                            @CSRF
                            <input type="hidden" name="category_id" value="{{$food->category->id}}">
                            <input type="hidden" name="food_id" value="{{$food->id}}">
                            <input type="hidden" name="restaurant_id" value="{{$food->restaurant->id}}">
                            <select name="rate" class="form-control">
                                @for($i = 1; $i<=5; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                            <button class="btn btn-light">Submit Rate</button>
                        </form>
                    @endauth
                </div>
            </div>
        @endforeach
    </div>

    <br>

    <div class="row">
        {{$foods->links()}}
    </div>
@endsection
