@extends("layouts.classresTemplate")

@section('title') Restaurant List @endsection

@section('content')
    <form method="GET">
        <div class="row">
            <div class="col-auto">
                <label for="city_id"><h5>City</h5></label><br>
                <select class="form-control" name="city_id">
                        <option value="">All Cities</option>
                    @foreach($cities as $city)
                        <option {{(request('city_id') == $city->id ? 'selected' : '')}}
                                value="{{$city->id}}">{{$city->title}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="col-auto">
                <label for="category_id"><h5>Category</h5></label><br>
                <select class="form-control" name="category_id">
                         <option value="">All Categories</option>
                    @foreach($categories as $category)
                        <option {{(request('category_id') == $category->id ? 'selected' : '')}}
                                value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-auto">
                <label for="textq"><h5>Address</h5></label><br>
                <input value="{{request('textq')}}" class="form-control" type="text" name="textq">
            </div>

            <div class="col-auto">
                <label for="is_open"><h5>Open</h5></label><br>
                <input {{(request('is_open') == 'on' ? 'checked' : '')}} class="form-control" type="checkbox" name="is_open">
            </div>

            <div class="col-auto">
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <button type="submit" class="btn btn-success">Filter</button>
                </div>
            </div>

        </div>
    </form><br>

    <div class="row">
        @foreach($restaurants as $restaurant)
            <div class="col col-md-2">
                <div class="card card-group">
                    <img alt="{{$restaurant->title}}" class="img-fluid img-thumbnail card-img-top"
                         width="200px" src="{{url('/storage/'.$restaurant->logo)}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$restaurant->title}}</h5>
                        <p class="card-text">Address : {{$restaurant->address}}<br>
                            Tel : {{$restaurant->phone}}<br>
                            Time : {{$restaurant->opening_time}} to {{$restaurant->closing_time}}<br>
                            City : {{$restaurant->city->title}}</p>
                        @if(now()->format('H') >= $restaurant->opening_time && now()->format('H') <= $restaurant->closing_time)
                            <span class="badge badge-success">Is Open</span>
                            <a href="{{route('user.showrestaurant',['id'=>$restaurant->id])}}">
                                <button type="submit" class="btn btn-primary">Order Now!</button>
                            </a>
                        @else
                            <span class="badge badge-danger">Is Closed</span>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div><br>

    <div class="row">
        <div class="col">
            {{$restaurants->links()}}
        </div>
    </div>

        <div class="row">
            <div class="col">
                <h2>Categories</h2>
            </div>
        </div>

        <div class="row">
            @foreach($categories as $category)
                <div class="col-auto">
                    <a href="{{route('user.category',['category_id'=>$category->id])}}">
                        <h4>{{$category->title}}</h4>
                    </a>
                </div>
            @endforeach
        </div>
@endsection
