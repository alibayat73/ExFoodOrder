@extends('layouts.classresTemplate')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Empty Cart!</h5>
            <p class="card-text">Your Cart is Empty. Nothing to be Showed Here. You May Want to Add a Food! Check Our Restaurant List.</p>
            <a href="{{route('user.indexrestaurant')}}" class="btn btn-primary">Restaurant List</a>
        </div>
    </div>
@endsection
