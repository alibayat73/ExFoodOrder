@extends("layouts.classresTemplate")

@section('title')  @endsection

@section('content')

    <div class="row">
        @if(session()->has('done'))
            <div class="table">
                <div class="alert alert-success">
                    {{session('done')}}
                </div>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-auto">
            <img alt="{{$restaurant->title}}" class="img-fluid img-thumbnail" height="150px" width="150px"
                 src="{{url('/storage/'.$restaurant->logo)}}">
                <h2>{{$restaurant->title}}</h2>
                <p>
                    Address : {{$restaurant->address}}<br>
                    Tel : {{$restaurant->phone}}<br>
                    Time : {{$restaurant->opening_time}} to {{$restaurant->closing_time}}<br>
                    City : {{$restaurant->city->title}}<br>
                </p>
        </div>
        <div class="col-auto">
            <div class="row">
                @foreach($foods as $food)
                    <div class="col-auto">
                        <div class="row">
                            @if(App\FoodImage::where('food_id',$food->id)->first() != null)
                                @foreach(\App\FoodImage::where('food_id',$food->id)->get() as $food_image)
                                    <img class="img-fluid img-thumbnail" width="200px" src="{{url('/storage/'.$food_image->path)}}">
                                    <br>
                                @endforeach
                            @else
                                <img class="img-fluid img-thumbnail" width="200px" src="{{url('nophoto.png')}}"> <br>
                            @endif
                        </div>
                        <div class="row">
                            <h5>{{$food->title}} : {{$food->price}}</h5>
                        </div>
                        @auth
                            <div class="row">
                                <a href="{{route('user.addtocart',['food_id'=>$food->id])}}">
                                    <button type="submit" class="btn btn-primary">Add To Cart</button>
                                </a><br>
                            </div>
                        @endauth
                        <div class="row">
                            <h5>Average = {{\App\FoodRate::where('food_id',$food->id)->avg('rate')}}</h5>
                        </div>
                        <div class="row">
                            @auth
                                <form action="{{route('user.rate')}}" method="POST">
                                    @CSRF
                                    <input type="hidden" name="category_id" value="{{$food->category->id}}">
                                    <input type="hidden" name="food_id" value="{{$food->id}}">
                                    <input type="hidden" name="restaurant_id" value="{{$restaurant->id}}">
                                    <select name="rate" class="form-control">
                                        @for($i = 1; $i<=5; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <button class="btn btn-light">Submit Rate</button>
                                </form>
                            @endauth
                        </div>
                    </div>
                @endforeach
            </div>

                <div class="row">
                    {{$foods->links()}}
                </div>
        </div>
    </div>

@endsection
