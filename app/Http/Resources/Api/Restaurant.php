<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class Restaurant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'Restaurant Name'=>$this->title,
            "Restaurant Address"=>$this->address,
            "Phone"=>$this->phone,
            "City"=>$this->city->id,
            "Opening_time"=>$this->opening_time,
            "Closing_time"=>$this->closing_time,
        ];
    }
}
