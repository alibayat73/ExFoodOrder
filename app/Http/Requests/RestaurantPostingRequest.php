<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantPostingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>['min:2','max:15','required'],
            'address'=>['min:10','max:150','required'],
            'phone'=>['min:8','max:11','required'],
            'opening_time'=>['min:1','max:2','required'],
            'closing_time'=>['min:1','max:2','required']
        ];
    }
}
