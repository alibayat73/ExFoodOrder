<?php

namespace App\Http\Controllers\Admin;

use App\Food;
use App\FoodImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\FoodPostingRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class FoodController extends Controller
{
    /**
     * Display A list of all Foods
     * @return Application|Factory|View
     */
    public function index()
    {
        $foods = Food::orderBy('id','asc')->get();
        return view('admin.food.indexfood',compact('foods'));
    }

    /**
     * Store a Food
     * @param FoodPostingRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(FoodPostingRequest $request)
    {
        Food::create
        ([
            'title' => $request->title,
            'price' => $request->price,
            'restaurant_id' => $request->restaurant_id,
            'category_id' => $request->category_id
        ]);

        session()->flash('done', 'Food Saved !');

        return redirect(route('admin.indexfood'));
    }

    /**
     * Show Edit blade
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $food = Food::find($id);
        return view('admin.food.editfood', compact('food'));
    }

    /**
     * Update A Food
     * @param FoodPostingRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(FoodPostingRequest $request,$id)
    {
        Food::find($id)->update
        ([
            'title' => $request->title,
            'price' => $request->price,
            'restaurant_id' => $request->restaurant_id,
            'category_id' => $request->category_id
        ]);

        session()->flash('done', 'Food Updated !');

        return redirect(route('admin.indexfood'));
    }

    /**
     * Delete A Food
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        Food::find($id)->delete();

        session()->flash('deleted', 'Food Deleted !');

        return redirect(route('admin.indexfood'));
    }

    /**
     * Show Upload Picture blade
     * @param $food_id
     * @return Application|Factory|View
     */
    public function upload($food_id)
    {
        $food = Food::find($food_id);
        return view('admin.food.image.uploadfoodimage', compact('food'));
    }

    /**
     * Save Uploaded Picture For A Food
     * @param $food_id
     * @return Application|RedirectResponse|Redirector
     */
    public function save($food_id)
    {
        FoodImage::create
        ([
            'path'=>request()->file('food_image')->store('Food Image','public'),
            'food_id'=>$food_id
        ]);

        return redirect(route('admin.indexfood'));
    }
}
