<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryPostingRequest;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Display list of all categories
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index()
    {
        try {
            $categories = Category::all();
        }catch (Exception $exception){
           throw $exception;
        }

        return view('admin.category.indexcategory',compact('categories'));
    }

    /**
     * Storing A New Category
     * @param CategoryPostingRequest $request
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function store(CategoryPostingRequest $request)
    {
        try {
            DB::beginTransaction();
            Category::create
            ([
                'title'=>$request->title
            ]);
            DB::commit();
            session()->flash('done','Category Saved !');
        }catch (Exception $exception){
            DB::rollBack();
            throw $exception;
        }

        return redirect(route('admin.indexcategory'));
    }

    /**
     * Show Edit blade
     * @param $id
     * @return Application|Factory|View
     * @throws Exception
     */
    public function edit($id)
    {
        try {
            $category = Category::find($id);
        }catch (Exception $exception){
            throw $exception;
        }
        return view('admin.category.editcategory',compact('category'));
    }

    /**
     * Update A Category
     * @param CategoryPostingRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function update(CategoryPostingRequest $request,$id)
    {
        try {
            DB::beginTransaction();
            Category::find($id)->update
            ([
                'title'=>$request->title
            ]);
            DB::commit();
            session()->flash('done','Category Updated !');
        }catch (Exception $exception){
            DB::rollBack();
            throw $exception;
        }

        return redirect(route('admin.indexcategory'));
    }

    /**
     * Delete A category
     * @param $id
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Category::find($id)->delete();
            DB::commit();
            session()->flash('deleted',"Category Deleted");
        }catch (Exception $exception){
            DB::rollBack();
            throw $exception;
        }

        return redirect(route('admin.indexcategory'));
    }
}
