<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * * Display A list of Users
     * @return Application|Factory|View
     */
    public function index()
    {
        $users = User::get();
        return view('admin.user.indexuser',compact('users'));
    }

    /**
     * Show Edit blade
     * @param $user_instance
     * @return Application|Factory|View
     */
    public function edit($user_instance)
    {
        $user = User::find($user_instance->id);
        return view('admin.user.edituser',compact('user'));
    }

    /**
     * Update A user
     * @param $user_instance
     * @return Application|RedirectResponse|Redirector
     */
    public function update($user_instance)
    {
        User::find($user_instance->id)->update
        ([
            'name'=>request('name'),
            'email'=>request('email'),
            'mobile'=>request('mobile'),
            'status'=>request('status'),
            'role_id'=>request('role_id'),
            'address'=>request('address')
        ]);

        return redirect(route('admin.indexuser'));
    }

    /**
     * Delete A user
     * @param $user_instance
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy($user_instance)
    {
        User::find($user_instance->id)->delete();
        return redirect(route('admin.indexuser'));
    }
}
