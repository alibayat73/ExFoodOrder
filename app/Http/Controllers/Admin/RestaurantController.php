<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RestaurantPostingRequest;
use App\Restaurant;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class RestaurantController extends Controller
{
    /**
     * Display A list of all restaurants
     * @return Application|Factory|View
     */
    public function index()
    {
        $restaurants = Restaurant::get();
        return view('admin.restaurant.indexrestaurant',compact('restaurants'));
    }

    /**
     * Store A new Restaurant
     * @param RestaurantPostingRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(RestaurantPostingRequest $request)
    {
        Restaurant::create
        ([
            'title'=>$request->title,
            'logo'=>request()->file('logo')->store('Logo','public'),
            'address'=>$request->address,
            'phone'=>$request->phone,
            'city_id'=>$request->city_id,
            'opening_time'=>$request->opening_time,
            'closing_time'=>$request->closing_time
        ]);

        session()->flash('done','Restaurant Saved !');

        return redirect(route('admin.indexrestaurant'));
    }

    /**
     * Show Edit blade
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $restaurant = Restaurant::find($id);
        return view('admin.restaurant.editrestaurant',compact('restaurant'));
    }

    /**
     * Update A Restaurant
     * @param RestaurantPostingRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(RestaurantPostingRequest $request,$id)
    {
        Restaurant::find($id)->update
        ([
            'title'=>$request->title,
            'logo'=>request()->file('logo')->store('Logo','public'),
            'address'=>$request->address,
            'phone'=>$request->phone,
            'city_id'=>$request->city_id,
            'opening_time'=>$request->opening_time,
            'closing_time'=>$request->closing_time
        ]);

        session()->flash('done','Restaurant Updated !');

        return redirect(route('admin.indexrestaurant'));
    }

    /**
     * Delete A Restaurant
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        Restaurant::find($id)->delete();

        session()->flash('deleted',"Restaurant Deleted");

        return redirect(route('admin.indexrestaurant'));
    }
}
