<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\CityPostingRequest;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CityController extends Controller
{
    /**
     * Display A list of all cities
     * @return Application|Factory|View
     */
    public function index()
    {
        $cities = City::all();
        return view('admin.city.indexcity',compact('cities'));
    }

    /**
     * Store A New City
     * @param CityPostingRequest $request
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function store(CityPostingRequest $request)
    {
        try {
            DB::beginTransaction();
            City::create
            ([
                'title'=>$request->title
            ]);
            DB::commit();
            session()->flash('done',"Adding Was Successful :)");
        }catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        return redirect(route('admin.indexcity'));
    }

    /**
     * Show Edit blade
     * @param $id
     * @return Application|Factory|View
     * @throws Exception
     */
    public function edit($id)
    {
        try {
            $city = City::find($id);
        }catch (Exception $exception){
            throw $exception;
        }
        return view('admin.city.editcity',compact('city'));
    }

    /**
     * Update A City
     * @param CityPostingRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function update(CityPostingRequest $request,$id)
    {
        try {
            DB::beginTransaction();
            City::find($id)->update
            ([
                'title'=>$request->title
            ]);
            DB::commit();
            session()->flash('done',"Editing Was Successful :)");
        } catch (Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        return redirect(route('admin.indexcity'));
    }

    /**
     * Delete on of cities
     * @param $id
     * @return Application|RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            City::find($id)->delete();
            DB::commit();
            session()->flash('deleted',"City Deleted");
        }catch (Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        return redirect(route('admin.indexcity'));
    }
}
