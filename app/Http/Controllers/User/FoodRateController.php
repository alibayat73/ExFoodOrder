<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\FoodRate;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FoodRateController extends Controller
{
    public function rate()
    {
        $food_id = request('food_id');
        $rate = request('rate');
        $user_id = Auth::id();
        $category_id = request('category_id');
        $restaurant_id = request('restaurant_id');

        if (FoodRate::where
        ([
            ['user_id',$user_id],
            ['food_id',$food_id]
        ])->exists())
        {
            FoodRate::where('food_id',$food_id)->update
            ([
                'rate'=>$rate,
            ]);

            return redirect(route('user.showrestaurant',['id'=>$restaurant_id]))->with('done','You Updated Your Rate :)');
        }
        else
        {
            FoodRate::create
            ([
                'rate'=>$rate,
                'user_id'=>$user_id,
                'food_id'=>$food_id,
                'category_id'=>$category_id
            ]);
        }

        return redirect(route('user.showrestaurant',['id'=>$restaurant_id]))->with('done','You Rated The Food :)');
    }
}
