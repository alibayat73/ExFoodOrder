<?php

namespace App\Http\Controllers\User;

use App\Cart;
use App\Food;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function add($food_id)
    {
        Cart::create
        ([
            'user_id'=> Auth::id(),
            'food_id'=>$food_id
        ]);

        return back()->with('done','Item Added To Cart Successfully');
    }

    public function index()
    {
        if(Cart::where('user_id',Auth::id())->exists())
        {
            $carts = Cart::where('user_id',Auth::id())->get();
            return view('user.restaurant.cart',compact('carts'));
        }
        return view('user.restaurant.emptycart');
    }

    public function destroyitem()
    {
        Cart::find(request('cart_item'))->delete();
        session()->flash('deleted','Item Was Removed');
        return redirect(route('user.cart'));
    }

    public function destroyall()
    {
        Cart::where('user_id',Auth::id())->delete();
        session()->flash('all_items_deleted','All Items Were Removed');
        return redirect(route('user.cart'));
    }

    public function storeOrder()
    {
        $user_id = Auth::id();
        $cart = session('cart');

        $order = Order::create
        ([
            'user_id'=>$user_id,
        ]);

        foreach ($cart as $food_id)
        {
            OrderItem::create
            ([
                'order_id'=>$order->id,
                'food_id'=>$food_id,
                'price'=>Food::find($food_id)->price
            ]);
        }

        session()->flush();
        session()->flash('submited','Order Submited.');
        return redirect(route('user.cart'));
    }
}
