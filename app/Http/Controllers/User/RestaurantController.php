<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\City;
use App\Food;
use App\Http\Controllers\Controller;
use App\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    public function index()
    {
        $restaurants = new Restaurant();

        if (request('city_id') != null)
            $restaurants = $restaurants->where('city_id',request('city_id'));

        if (request('is_open') == 'on')
        {
            $current_time = now()->format('H');
            $restaurants = $restaurants->where('opening_time','<=',$current_time);
            $restaurants = $restaurants->where('closing_time','>=',$current_time,);
        }

        if (request('textq') != null)
        {
            $restaurants = $restaurants->where(function($query)
            {
                $query->orWhere('title','like','%'.request('textq').'%')
                    ->orWhere('address','like','%'.request('textq').'%');
            });
        }

        if (request('category_id') != null)
        {
            $restaurants = $restaurants->whereHas('foods',function($query)
            {
                $query->where('category_id',request('category_id'));
            });
        }

        $restaurants = $restaurants->paginate(5);
        $cities = City::all();
        $categories = Category::all();

        return view('user.restaurant.indexrestaurant',compact('restaurants','categories','cities'));
    }

    public function show($id)
    {
        $restaurant = Restaurant::find($id);
        $foods = Food::where('restaurant_id',$id)->orderBy('category_id','asc')->paginate(2);
        return view('user.restaurant.order',['restaurant'=>$restaurant,'foods'=>$foods]);
    }
}
