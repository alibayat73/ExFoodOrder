<?php

namespace App\Http\Controllers\User\Api;

use App\Food;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Food as FoodResource;
use App\Http\Resources\Api\Restaurant as RestaurantResource;
use App\Restaurant;

class RestaurantController extends Controller
{
    public function index()
    {
        $restaurants = new Restaurant();

        if (request('city_id') != null)
            $restaurants = $restaurants->where('city_id',request('city_id'));

        if (request('is_open') == 'on')
        {
            $current_time = now()->format('H');
            $restaurants = $restaurants->where('opening_time','<=',$current_time);
            $restaurants = $restaurants->where('closing_time','>=',$current_time,);
        }

        if (request('textq') != null)
        {
            $restaurants = $restaurants->where(function($query)
            {
                $query->orWhere('title','like','%'.request('textq').'%')
                    ->orWhere('address','like','%'.request('textq').'%');
            });
        }

        if (request('category_id') != null)
        {
            $restaurants = $restaurants->whereHas('foods',function($query)
            {
                $query->where('category_id',request('category_id'));
            });
        }

        $restaurants = $restaurants->all();
        return RestaurantResource::collection($restaurants);
    }

    public function show($restaurant_id)
    {
        $foods = Food::where('restaurant_id',$restaurant_id)->get();
        return FoodResource::collection($foods);
    }
}
