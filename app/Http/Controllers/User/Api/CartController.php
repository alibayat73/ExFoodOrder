<?php

namespace App\Http\Controllers\User\Api;

use App\Cart;
use App\Food;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\Cart as CartResource ;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function add($food_id)
    {
        return Cart::create
        ([
            'user_id'=> Auth::id(),
            'food_id'=>$food_id
        ]);
    }

    public function index()
    {
        $cart = Cart::where('user_id',Auth::id())->get();
        return CartResource::collection($cart);
    }

    public function destroyitem($food_id)
    {
        function cartItem($food_id)
        {
            return
                Cart::where([
                    ['user_id','=',Auth::id()],
                    ['food_id','=',$food_id]
                ]);
        }
        $name = Auth::user()['name'];
        $food_name = Food::find($food_id)->title;

        if(cartItem($food_id)->exists())
        {
            cartItem($food_id)->delete();
            return "Dear $name, $food_name is now deleted.";
        }
        else
            return "Dear $name, $food_name is not in your cart.";
    }

    public function destroyall()
    {
        function allCartItems()
        {
            return Cart::where('user_id','=',Auth::id());
        }
        $name = Auth::user()['name'];

        if(allCartItems()->exists())
        {
            allCartItems()->delete();
            return "Dear $name, your cart has got empty.";
        }
        else
            return "Dear $name, your cart is already empty!";
    }

    public function storeOrder()
    {
        $user_id = Auth::id();
        $cart = session('cart');

        $order = Order::create
        ([
            'user_id'=>$user_id,
        ]);

        foreach ($cart as $food_id)
        {
            OrderItem::create
            ([
                'order_id'=>$order->id,
                'food_id'=>$food_id,
                'price'=>Food::find($food_id)->price
            ]);
        }

        session()->flush();
        session()->flash('submited','Order Submited.');
        return redirect(route('user.cart'));
    }
}
