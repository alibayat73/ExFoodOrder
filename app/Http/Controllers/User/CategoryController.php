<?php

namespace App\Http\Controllers\User;

use App\Food;
use App\Http\Controllers\Controller;
use App\Restaurant;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show($category_id)
    {
        $foods = Food::where('category_id',$category_id)->paginate(2);
        return view('user.restaurant.foodsbycategory',compact('foods'));
    }
}
