<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        request()->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $credentials = request()->only('email','password');
        if(Auth::attempt($credentials))
        {
            $user = auth()->user();
            return ['status'=>true,
                    'message'=>"Login Succesfull.",
                    'Token'=> $user->createToken('Login')->accessToken];
        }
        return ['status'=>false,
                'message'=>'Login Not Succesfull.'];
    }
}
