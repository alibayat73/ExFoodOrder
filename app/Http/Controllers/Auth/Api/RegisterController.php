<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\UserRegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register()
    {
        request()->validate
        ([
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'numeric'],
            'address' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        User::create
        ([
            'name' => request()->name,
            'mobile' => request()->mobile,
            'address' => request()->address,
            'email' => request()->email,
            'password' => Hash::make(request()->password),
        ]);

        return ['status'=>true,
            'message'=>'Sign Up Successful'];
    }

}
