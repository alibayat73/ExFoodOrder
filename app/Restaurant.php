<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $guarded = [];

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function foods()
    {
        return $this->hasMany('App\Food');
    }
}
