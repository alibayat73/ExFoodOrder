<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Authorizing application administrators to perform any action.
     *
     * @param User $user
     * @return mixed
     */
    public function before(User $user)
    {
        if ($user->role_id === 0)
            return true;
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(?User $user)
    {
        if (optional($user)->role_id === 0)
            return Response::allow();
        else
            return Response::deny('You Can Not See Other User Accounts.');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function update(?User $user, User $model)
    {
        if (optional($user)->id === $model->id)
            return Response::allow();
        else
            return Response::deny('You Can Not Edit Other User Accounts.');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function forceDelete(?User $user, User $model)
    {
        if (optional($user)->id === $model->id)
            return Response::allow();
        else
            return Response::deny('You Can Not delete Other User Accounts.');
    }
}
