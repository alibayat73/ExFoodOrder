<?php

use App\User;

Route::view('','FirstPage')->name('first');

Route::prefix('admin')->group(function ()
{
    //Home Page(Admin Panel)
    Route::view('','admin.homepage')->name('admin');

    Route::prefix('city')->group(function ()
    {
        Route::GET('','Admin\CityController@index')->name('admin.indexcity');
        Route::view('create','admin.city.createcity')->name('admin.createcity');
        Route::POST('','Admin\CityController@store')->name('admin.storecity');
        Route::GET('edit/{id}','Admin\CityController@edit')->name('admin.editcity');
        Route::PUT('{id}','Admin\CityController@update')->name('admin.updatecity');
        Route::DELETE('destroy/{id}','Admin\CityController@destroy')->name('admin.destroycity');
    });

    Route::prefix('restaurant')->group(function ()
    {
        Route::GET('','Admin\RestaurantController@index')->name('admin.indexrestaurant');
        Route::view('create','admin.restaurant.createrestaurant')->name('admin.createrestaurant');
        Route::POST('','Admin\RestaurantController@store')->name('admin.storerestaurant');
        Route::GET('edit/{id}','Admin\RestaurantController@edit')->name('admin.editrestaurant');
        Route::PUT('{id}','Admin\RestaurantController@update')->name('admin.updaterestaurant');
        Route::DELETE('destroy/{id}','Admin\RestaurantController@destroy')->name('admin.destroyrestaurant');
    });

    Route::prefix('category')->group(function ()
    {
        Route::GET('','Admin\CategoryController@index')->name('admin.indexcategory');
        Route::view('create','admin.category.createcategory')->name('admin.createcategory');
        Route::POST('','Admin\CategoryController@store')->name('admin.storecategory');
        Route::GET('edit/{id}','Admin\CategoryController@edit')->name('admin.editcategory');
        Route::PUT('{id}','Admin\CategoryController@update')->name('admin.updatecategory');
        Route::DELETE('destroy/{id}','Admin\CategoryController@destroy')->name('admin.destroycategory');
    });

    Route::prefix('food')->group(function ()
    {
        Route::GET('','Admin\FoodController@index')->name('admin.indexfood');
        Route::view('create','admin.food.createfood')->name('admin.createfood');
        Route::POST('','Admin\FoodController@store')->name('admin.storefood');
        Route::GET('edit/{id}','Admin\FoodController@edit')->name('admin.editfood');
        Route::PUT('{food_id}','Admin\FoodController@update')->name('admin.updatefood');
        Route::DELETE('destroy/{id}','Admin\FoodController@destroy')->name('admin.destroyfood');
        Route::POST('uploadfoodimage/{food_id}','Admin\FoodController@upload')->name('admin.uploadfoodimage');
        Route::POST('savefoodimage/{food_id}','Admin\FoodController@save')->name('admin.savefoodimage');
    });

    Route::prefix('user')->group( function ()
    {
        Route::GET('','Admin\UserController@index')->name('admin.indexuser');
        //->middleware('can:viewAny,App\User');

        Route::GET('edit/{user:id}','Admin\UserController@edit')->name('admin.edituser')
        ->middleware('can:update,user');

        Route::PUT('{user:id}','Admin\UserController@update')->name('admin.updateuser')
        ->middleware('can:update,user');

        Route::DELETE('destroy/{user:id}','Admin\UserController@destroy')->name('admin.destroyuser')
        ->middleware('can:forceDelete,user');
    });

});

//------------------------------------------------------------------------------------------------------

Route::prefix('user/restaurant')->group( function (){
    Route::GET('','User\RestaurantController@index')->name('user.indexrestaurant');
    Route::GET('show/{id}','User\RestaurantController@show')->name('user.showrestaurant');
});


Route::GET('AddToCart/food/{food_id}','User\CartController@add')->name('user.addtocart');
Route::prefix('cart')->group( function (){
    Route::GET('','User\CartController@index')->name('user.cart');
    Route::DELETE('','User\CartController@destroyitem')->name('user.cartdestroyitem');
    Route::POST('','User\CartController@destroyall')->name('user.cartdestroyall');
    Route::POST('submit','User\CartController@storeOrder')->name('user.cartsubmit');
});

//User Category Routes
Route::GET('user/category/{category_id}','User\CategoryController@show')->name('user.category');

//User Foodrates Routes
Route::POST('rate','User\FoodRateController@rate')->name('user.rate');

//------------------------------------------------------------------------------------------------------

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
