<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Admin')->group(function (){
    Route::namespace('Api')->group(function (){

    });
});

Route::namespace('Auth')->group(function (){
    Route::namespace('Api')->group(function (){
        Route::POST('userRegister', 'RegisterController@register');
        Route::POST('userLogin', 'LoginController@login');
    });
});

Route::namespace('User')->group(function (){
    Route::namespace('Api')->group(function (){
        Route::group(['Restaurant'],function () {
            Route::POST('restaurant', 'RestaurantController@index');
            Route::POST('restaurant/{restaurant_id}', 'RestaurantController@show');
        });
        Route::group(['Cart'], function () {
            Route::POST('cart','CartController@index')->middleware('auth:api');
            Route::POST('cart/add/{food_id}','CartController@add')->middleware('auth:api');
            Route::POST('cart/delete/{food_id}','CartController@destroyitem')->middleware('auth:api');
            Route::POST('cart/deleteAll','CartController@destroyall')->middleware('auth:api');
        });
    });
});
